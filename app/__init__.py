#-*- coding:utf-8 -*-

from flask import Flask
from app.database import db
from app.celery import celery

def create_app():
    app = Flask(__name__)
    app.config.from_object('config.Config')
    db.init_app(app)
    celery.conf.update(app.config)    

    with app.app_context():
        from app.routes import home

    @app.before_first_request
    def initialize_database():
        db.create_all()

    @app.errorhandler(404)
    def not_found(error):
        return '404 not found', 404

    @celery.task()
    def add(a, b):
        print(a + b)
        return a + b

    return app
