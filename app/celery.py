#-*- coding:utf-8 -*-

from celery import Celery
import os

CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379')
RESULT_BACKEND = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379')
print("UUUU:", CELERY_BROKER_URL)

celery = Celery(__name__, broker=CELERY_BROKER_URL, result_backend=RESULT_BACKEND)

# def make_celery(app):
#     celery = Celery(
#         app.import_name,
#         backend=app.config['CELERY_RESULT_BACKEND'],
#         broker=app.config['CELERY_BROKER_URL']
#     )
#     celery.conf.update(app.config)
#
#     class ContextTask(celery.Task):
#         def __call__(self, *args, **kwargs):
#             with app.app_context():
#                 return self.run(*args, **kwargs)
#
#     celery.Task = ContextTask
#     return celery
