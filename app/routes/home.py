#-*- coding:utf-8 -*-

from flask import current_app as app
from flask import request, render_template
from app.tasks import celery_process

@app.route('/')
def index():
    return render_template('main.html', res="")

@app.route('/celery', methods=["GET", "POST"])
def celery_task():
    if request.method == 'GET':
        return render_template('main.html', res="")
    else:
        a = int(request.form["num_one"])
        b = int(request.form["num_two"])
        result = celery_process(a, b)
        print("Celery Result:", result)
        return render_template('main.html', res="="+str(result))
