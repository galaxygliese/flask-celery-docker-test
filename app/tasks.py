#-*- coding:utf-8 -*-

from app.celery import celery
import time

@celery.task(name='Sum of 2 number')
def celery_process(a, b):
    time.sleep(2)
    print("CELERY OK!")
    return a + b
