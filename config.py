import os

class Config:
    DEBUG = True
    SECRET_KEY = 'hardtoguess'

    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
